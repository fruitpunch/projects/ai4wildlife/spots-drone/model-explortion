# importing required libraries 
import numpy as np 
import cv2 
from time import time 
import tensorflow as tf
from tensorflow.keras.models import load_model 
from tensorflow.keras import Model, Input 
from tensorflow.keras.layers import Conv2D, Dense, Flatten, MaxPooling2D, BatchNormalization, Dropout, Rescaling
from tensorflow.keras.callbacks import ModelCheckpoint, EarlyStopping
import datetime 

# loading model 
model_filepath = 'trained_vggnet_model.h5'
model = load_model(model_filepath)

# opening video capture stream
invideo_filepath = 'video53_1.mp4'
vcap = cv2.VideoCapture(invideo_filepath)
fps    = int(vcap.get(5))
width  = int(vcap.get(3))
height = int(vcap.get(4))
print("Original video - width: {} , height: {} , fps: {}".format(width, height, fps))

# video writer 
outvideo_filepath = "inference_result_poacher_classification.mp4"
fourcc = cv2.VideoWriter_fourcc(*'XVID')
vout   = cv2.VideoWriter(outvideo_filepath, fourcc, fps, (width, height), True) # True/False for color/gray images respectively

# parameters for cropping frame (for consistency and removing black borders)
crop_h = 470
crop_w = 640
crop_offset_h = int((height - crop_h)//2)
crop_offset_w = int((width - crop_w)//2) 
crop_y_start  = crop_offset_h
crop_y_end    = crop_offset_h + crop_h
crop_x_start  = crop_offset_w
crop_x_end    = crop_offset_w + crop_w

# model input image parameters
model_input_h = 256
model_input_w = 256

# processing video 
start = time()
num_frames_processed = 0
while True :
    # reading frame 
    grabbed, orig = vcap.read() # frame is of dtype np.uint8 and with 3 channels 
    if grabbed is False :
        print('[Exiting] No more frames to read from stream')
        break 

    # pre-processing frame for inference
    # crop frame 
    frame = orig[crop_y_start:crop_y_end, crop_x_start:crop_x_end]
    # convert to grayscale 
    frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    # resize frame 
    frame = cv2.resize(frame, (model_input_w , model_input_h))
    # add both and channel dims 
    frame = frame.reshape((1,model_input_h, model_input_w,1))
    # Note that rescaling to 0-1 not required since rescaling is part of the model architecture

    # perform inference 
    pred = model(frame)[0].numpy()
    pred_label = 0 if pred < 0.5 else 1

    # computing current time and (dummy) GPS coordinates 
    curr_time = datetime.datetime.now()
    curr_time_str = '{}:{}:{}'.format(curr_time.hour, curr_time.minute, curr_time.second)

    # annotate frame
    if pred_label == 1 :
        cv2.putText(orig, curr_time_str, (10,20), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0,255,0), 2)
        cv2.putText(orig, 'GPS:()', (10,40), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0,255,0), 2)
        text = 'Detected Human (prob:{:.2f})'.format(float(pred))
        cv2.putText(orig, text, (10,60), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0,0,255), 2)
    else :
        cv2.putText(orig, curr_time_str, (10,20), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0,255,0), 2)
        cv2.putText(orig, 'GPS:', (10,40), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0,255,0), 2)

    # writing frame
    vout.write(orig)

    # incrementing count of frames processed
    num_frames_processed += 1

    # displaying frame 
    cv2.imshow('video' , orig)
    key = cv2.waitKey(1)
    if key == ord('q'):
        break

end = time()

# printing stats
time_elapsed = end - start 
fps = num_frames_processed/time_elapsed
print('time_elapsed: {}, num_frames_processed:{}, fps:{}'.format(time_elapsed, num_frames_processed, fps))

# releasing input stream , closing all windows 
vcap.release()
vout.release()
cv2.destroyAllWindows()
