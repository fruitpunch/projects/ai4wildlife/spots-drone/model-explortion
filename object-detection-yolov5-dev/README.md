This repository is used for storing the source code of the object detection model (yolov5).

## Original Repository for YOLO V5
Code from [the official repo maintained by ultralytics](https://github.com/ultralytics/yolov5).

## Install required libraries

To install required libraries run 

`pip3 install -r requirements.txt`

## Adaptation of YOLO V5 dataset

The dataset used for training the models was *produced in the first challenge of AI4W*, found here [AI4W Challenge 1 Dataset](https://drive.google.com/drive/folders/1REpcDeew7hqdAPCPpG_5pmrjuBkj5JzQ):
In order to use the dataset we need to modify it. 

1. Download the _Labeld**_ data from Google Drive and extract on your computer.
2. In the root of the dataset, eg. Labeld/, create a folder "labels/" and within create "test/", "train/", "val/" folders.  
3. Use [poachers_coco2yolo.py](data/poachers_coco2yolo.py) script to convert the annotations to yolo format. In the script change the root folder to point to where you have the data. Also we don’t gurantee that the needed folders will be created so you can create “c1data/labels”, “c1data/labels/train”, “c1data/labels/val/” and “c1data/labels/test” if c1data/ is your root folder of the downloaded data.
4. Place ai4w_c1data.yaml into the data/ folder.
5. Make sure the dataset is at the specified folder within ai4w_c1data.yaml. (You should have datasets/c1data/images/ and datasets/c1data/labels/ there).
6. You are good to go.

## Training the YOLO models on the C1 AI4W dataset

Training YOLO v5 after dataset preparation is quite easy, but you need to make a decision first.
You can select Yolo5 Nano, Yolo5 Small, Yolo5 Medium, Yolo5 Large and Yolo5 Extra Large as the model for training.
For the needs of the challenge Yolo5 Small seems to be high speed, ok accuracy and _Yolo5 Medium_ is the sweet spot - ok speed, high accuracy.

To train, simply issue the command:

`python train.py --img 640 --batch 16 --epochs 30 --data ai4w_c1data.yaml --weights yolov5m.pt --project "poachers" --name "medium_v60_yolov5_c1data"`

## Model validation and testing

To validate your trained model simply run

`python val.py --img 640 --data ai4w_c1data.yaml --weights poachers/medium_v60_yolov5_c1data/weights/best.pt`

## Rescale training job log
2021_11_14_16_41_7<br />
2021_11_15_15_18_19<br />
