import requests
import os
import datetime
import subprocess
import json

# credentials fetched from environment variables
RESCALE_API_KEY = os.environ.get('RESCALE_API_KEY')
GITLAB_USER_EMAIL = os.environ.get('GITLAB_USER_EMAIL')
GITLAB_USER_NAME = os.environ.get('GITLAB_USER_NAME')
GIT_BRANCH = os.environ.get('CI_COMMIT_REF_NAME')
GIT_PWD = os.environ.get('GIT_PWD') # TODO: see if we can replace it with a token
GIT_USR = os.environ.get('GITLAB_USER_LOGIN')

# use timestamp identify the current job
timestamp = (f'{datetime.datetime.now().year}_'
              f'{datetime.datetime.now().month}_'
              f'{datetime.datetime.now().day}_'
              f'{datetime.datetime.now().hour}_'
              f'{datetime.datetime.now().minute}_'
              f'{datetime.datetime.now().second}')

# you can set up post processing script file here, check the shell script referenced for details
response = subprocess.run("utils/rescale/postproc_rescale.sh", capture_output=True)
script_id = json.loads(response.stdout)['id'] # this id is used later in job creation to specify the script to be used

# POST request to create the job
response = requests.post(
  'https://eu.rescale.com/api/v2/jobs/',
  headers={'Content-Type': 'application/json',
           'Authorization': f'Token {RESCALE_API_KEY}'},
  json={
      'name': f'yolov5_{timestamp}',
      'jobanalyses': [
          {
              'analysis': {
                  'code': 'anaconda', # using anaconda to install and use python libraries
              },
              'command': (
                  f'git clone https://{GIT_USR}:{GIT_PWD}@gitlab.com/fruitpunch'
                  '/projects/ai4wildlife/spots-drone/poacher-detection/object-detection-yolov5.git &&' # clone the repo
                  'cd object-detection-yolov5 &&' 
                  f'git checkout {GIT_BRANCH} &&' # switch to current branch
                  'pip install -r utils/rescale/requirements.txt &&' # install requirements
                  f'echo "{timestamp}<br />" >> README.md' # this is a dummy command, actual training scripts should be run here
                  ),
              'hardware': { # configure the hardware on which to run the job
                  'coreType': 'emerald',
                  'coresPerSlot': 1
              },
              'postProcessScript': {
                  'id': script_id # specify script file to be used in post processing, id fetched above
              },
              'postProcessScriptCommand': ('cd object-detection-yolov5 &&'
                                           f'git config --global user.email "{GITLAB_USER_EMAIL}" &&' 
                                           f'git config --global user.name "{GITLAB_USER_NAME}" &&'
                                           'git add . &&'
                                           f'git commit -m "training job finished" &&'
                                           'git push')
          }
      ]
  }
)

print(response)

# fetch the job id returned in the response
job_id = json.loads(response.text)['id']

response = requests.post(
  f'https://eu.rescale.com/api/v2/jobs/{job_id}/submit/',
  headers={'Content-Type': 'application/json',
           'Authorization': f'Token {RESCALE_API_KEY}'}
           )

print(response)