#!/bin/bash
curl -X POST -H 'Content-Type:multipart/form-data' \
     -H "Authorization: Token ${RESCALE_API_KEY}" \
     -F 'file=@detect.py' \
     -F 'type_id=4' \
     https://eu.rescale.com/api/v2/files/contents/