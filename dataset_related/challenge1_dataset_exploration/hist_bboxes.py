# program to plot a histogram of the number of (poacher) bounding boxes in each image 

# importing required libraries 
import json 
import matplotlib.pyplot as plt 

# defining input parameters 
output_plotpath        = 'hist_bbs_count.jpg'
simple_annots_filepath = 'instances_train_simplified.json'

# loading simple annotations json file 
with open(simple_annots_filepath, 'r') as fh :
    content = json.load(fh)

# computing number of bounding boxes in each image 
annotations = content['annotations']
counts = [] # list storing number of bounding boxes in each image 
for filename, values_dict in annotations.items():
        bboxes = values_dict['bboxes']
        num_bboxes = len(bboxes)
        counts.append(num_bboxes)

# plotting histogram 
bins = list(range(max(counts)+2))
plt.hist(counts, bins=bins, align='left')
plt.title("Histogram of (poacher) bounding box counts in training dataset")
plt.xlabel('Number of bounding boxes in each image')
plt.ylabel('Number of images')
plt.xticks(bins)
plt.savefig(output_plotpath)
plt.show()

