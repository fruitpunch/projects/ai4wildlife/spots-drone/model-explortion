# Run as python visualize_bboxes.py 
# Python 3.x is used 
# program to visualize the bounding boxe annotations for a given image 
# input parameters are input image path (including folder location or relative path), input image name, 
# output image path 
# path to the simplified annotations json file 
# image name in the input parameter and annotations file must match 
# make sure that the correct pair of image and annotation file is provided. So , if a training image is used , then training annotations file must be provided

# importing required libraries 
import cv2 
import json 

# defining input parameters 
image_filepath         = 'videobh_41_000343.PNG' # input image filepath including folder(or relative filepath) and filename  
image_filename         = 'videobh_41_000343.PNG' # just the input image filename . manually provided this to avoid parsing filepath which may have OS dependencies
output_filepath        = 'annotated_videobh_41_000343.PNG'
simple_annots_filepath = 'instances_train_simplified.json'

# loading simple annotations json file 
with open(simple_annots_filepath, 'r') as fh :
    content = json.load(fh)

annotations = content['annotations']
found = False
for filename, values_dict in annotations.items():
    if filename == image_filename :
        found = True 
        bboxes = values_dict['bboxes']

if found is False :
    print("filename not found in annotations file. Exiting")
    exit()

image = cv2.imread(image_filepath)
print("image(numpy array) shape : {}".format(image.shape))
for bbox in bboxes :
    top_left = [int(bbox[0]), int(bbox[1])] # x,y coordinates of top left corner of the bounding box 
    bottom_right = [ int(bbox[0] + bbox[2]) , int(bbox[1] + bbox[3]) ]
    cv2.rectangle(image, top_left, bottom_right, (0,0,255), 1)
    #print(bbox)
cv2.imwrite(output_filepath, image)

